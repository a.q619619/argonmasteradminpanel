import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { url } from './variables/charts'
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AllSearvicesService {
  sub: any;
  // id: string;


  allCompanies = `${url}/admin/allCompanies`
  allServiceProvider = `${url}/admin/allServiceProviders`
  allBills = `${url}/admin/allBills`
  allSearches = `${url}/admin/allSearches`
  allRequest = `${url}/admin/allPasswordRequests`

  constructor(private http: HttpClient, private route: ActivatedRoute) { }



  getCompanies() {
    return this.http.get(this.allCompanies)
  }
  getServiceProvider() {
    return this.http.get(this.allServiceProvider)
  }
  getBillHistory() {
    return this.http.get(this.allBills)
  }
  getAllSearches() {
    return this.http.get(this.allSearches)
  }
  getAllRequest() {
    return this.http.get(this.allRequest)
  }
  getAllEmpolyee(id: any) {
    console.log('IDDD---->>', id)
    return this.http.get(`${url}/employee/employeesByCompany/${id}`)
  }
  AddCompany(data: any) {
    return this.http.post(
      `${url}/company/addCompany`,
      {
        companyName: data.company_name,
        city: data.company_city,
        email: data.email,
        address: data.company_adress,
        password: data.Password
      })
  }
  EditCompany(data: any,Items: any) {
    return this.http.put<any>(
      `${url}/company/updateCompany`,
      {
        id:Items._id,
        companyName: data.company_name,
        city: data.company_city,
        email: data.email,
        address: data.company_adress,
        password: data.Password
      })
  }
  login(data: any) {
    return this.http.post(
      `${url}/admin/loginAdmin`,
      {
        email: data.email,
        password: data.password
      })
  }
  AddServiceProvider(data: any) {
    return this.http.post(
      `${url}/serviceprovider/addServiceProvider`,
      {
        providerName: data.provider_name,
        city: data.provider_city,
        email: data.email,
        address: data.provider_adress,
        password: data.Password
      })
  }
 EditServiceProvider(data: any,Items: any) {
    return this.http.put<any>(
      `${url}/serviceprovider/updateServiceProvider`,
      {
        id:Items._id,
        providerName: data.provider_name,
        city: data.provider_city,
        email: data.email,
        address: data.provider_adress,
        password: data.Password
      })
  }

  AcceptCompaniesRequest(data: any) {
    return this.http.post(
      `${url}/admin/acceptPasswordRequest`,
      {
        id: data._id,
        requestType: data.requestType,
        newPassword: data.newPassword,
        company: data.company._id
      })
  }
  AcceptProviderRequest(data: any) {
    return this.http.post(
      `${url}/admin/acceptPasswordRequest`,
      {
        id: data._id,
        requestType: data.requestType,
        newPassword: data.newPassword,
        serviceProvider: data.serviceProvider._id
      })
  }
  RejectRequest(data: any) {
    return this.http.post(
      `${url}/admin/rejectPasswordRequest`,
      {
        id: data._id,
      
      })
  }
  ChangeCompanyStatus(data: any,statusCh: string) {
    return this.http.put<any>(
      `${url}/company/updateCompanyStatus`,
      {
        id: data,
        status: statusCh 
      
      })
  }
  ChangeProviderStatus(data: any,statusCh: string) {
    return this.http.put<any>(
      `${url}/serviceprovider/updateServiceProviderStatus`,
      {
        id: data,
        status: statusCh 
      
      })
  }
  ChangeEmpolyeeStatus(data: any,statusCh: string) {
    return this.http.put<any>(
      `${url}/employee/updateEmployeeStatus`,
      {
        id: data,
        status: statusCh 
      
      })
  }
}
