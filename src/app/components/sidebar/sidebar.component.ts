import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/companies', title: 'Comapnies',  icon:'ni-building text-blue', class: '' },
    { path: '/serviceProvider', title: 'Service Provider',  icon:'ni-map-big text-orange', class: '' },
    { path: '/billhistroy', title: 'Bill History',  icon:'fa fa-table text-yellow', class: '' },
    { path: '/searchEmpolyee', title: 'Search Empolyee',  icon:'fa fa-newspaper text-green', class: '' },
    { path: '/searches', title: 'All Searches',  icon:'ni-bullet-list-67 text-red', class: '' },
    { path: '/passwordRequest', title: 'Password Reguest',  icon:'fa fa-key  ', class: '' },
    // { path: '/login', title: 'Login',  icon:'ni-key-25 text-info', class: '' },
    // { path: '/register', title: 'Register',  icon:'ni-circle-08 text-pink', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
}
