import { Component, OnInit } from '@angular/core';
import { AllSearvicesService } from '../../all-searvices.service'
import { ActivatedRoute, Router } from '@angular/router'


@Component({
  selector: 'app-addemployee',
  templateUrl: './addemployee.component.html',
  styleUrls: ['./tables.component.scss']
})
export class AddEmpolyeeComponent {
  data: any;
  sub: any;
  Items: any;
  list: any[] = []

  constructor(private UserData: AllSearvicesService, private router: Router, private route: ActivatedRoute) {
    this.sub = this.route.params.subscribe(params => {
      this.Items = params;
    });
    console.log(this.Items);
  }
  addCompany(FormData: any) {
    console.log('FormData', FormData)
    if (this.Items.companyName) {
      this.UserData.EditCompany(FormData, this.Items).subscribe((response: any) => {
        console.log('Response Update Company ---->>>', response)
        if (response.message == 'Success') {
          this.router.navigate(['/companies']);
        }
      })
    }
    else {
      this.UserData.AddCompany(FormData).subscribe((response: any) => {
        console.log('Response Add Company ---->>>', response)
        if (response.message == 'Success') {
          this.router.navigate(['/companies']);
        }
        // this.data=response
      })
    }

  }
  handleChangeInput(event: any, index: number) {
    console.log(event,index);
    // let obj = this.list;
    // obj[index][event.target.name] = event.target.value;
    // console.log('obj', obj)


  }
  addList(data: any) {
    this.list.push({ Name: data.name, age: data.age })
    console.log(this.list)
  }
  ngOnInit() {


  }

}
