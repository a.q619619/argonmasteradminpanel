import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AllSearvicesService} from '../../all-searvices.service'
@Component({
  selector: 'app-passwordRequest',
  templateUrl: './passwordRequest.component.html',
  styleUrls: ['./tables.component.scss']
})
export class PasswordRequestComponent {
  active = 1;
  data: [];
  ComaniesRequest:[]
//  date:any;
  constructor(private UserData: AllSearvicesService,private router:Router) {  

  UserData.getAllRequest().subscribe((response:any)=>{
    console.log('Response All Request ---->>>', response)
    this.ComaniesRequest=response.doc.filter((item: { company: any; }) => item.company );
    this.data=response.doc.filter((item: { serviceProvider: any; }) => item.serviceProvider )
})
}

AcceptComStatus(data:any){
  console.log(data)
  this.UserData.AcceptCompaniesRequest(data).subscribe((response:any)=>{
    console.log('Response All AcceptComStatus  ---->>>', response)
})
}
RejectPasswordRequest(data:any){
  console.log(data)
  this.UserData.RejectRequest(data).subscribe((response:any)=>{
    console.log('Response All Reject  ---->>>', response)
})
}
AcceptProviderStatus(data:any){
  console.log(data)
  this.UserData.AcceptProviderRequest(data).subscribe((response:any)=>{
    console.log('Response All AcceptProviderStatus  ---->>>', response)
    this.UserData.getAllRequest().subscribe((response:any)=>{
      console.log('Response All Request ---->>>', response)
      this.ComaniesRequest=response.doc.filter((item: { company: any; }) => item.company );
      this.data=response.doc.filter((item: { serviceProvider: any; }) => item.serviceProvider )
  })
})
}
}

