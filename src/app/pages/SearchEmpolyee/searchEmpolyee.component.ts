import { Component, OnInit } from '@angular/core';
import {AllSearvicesService} from '../../all-searvices.service'
import { ActivatedRoute, Router } from '@angular/router'
import "bootstrap/dist/css/bootstrap.min.css";



@Component({
  selector: 'app-searchEmpolyee',
  templateUrl: './searchEmpolyee.component.html',
  styleUrls: ['./tables.component.scss']
})
export class SearchEmpolyeeComponent {
  // data: any;.
    sub: any;
    Items: any;
    selectedDevice:'Select'
  constructor(private UserData: AllSearvicesService,private router: Router,private route : ActivatedRoute ) {
    this.sub = this.route.params.subscribe(params => {
      this.Items = params;
      });
      console.log(this.Items);
    }
    addProvider(data:any){
      console.log('FormData',data)
      if(this.Items.providerName){
        this.UserData.EditServiceProvider(data,this.Items).subscribe((response:any)=>{
          console.log('Response Update Service Provider ---->>>',response)
          if(response.message == 'Success'){
            this.router.navigate(['/serviceProvider']);
          }
        })
      }
      else{
        this.UserData.AddServiceProvider(data).subscribe((response:any)=>{
          console.log('Response Add Service Provider ---->>>',response)
          if(response.message == 'Success'){
            this.router.navigate(['/serviceProvider']);
          }
        
        })
      }
     
     }
     onChangeSeclect(event: any){
      console.log('Select', event);
      this.selectedDevice=event;
     }

     ngOnInit() {
     
       
     }

    }
