import { Component, OnInit } from '@angular/core';
import {AllSearvicesService} from '../../all-searvices.service'
import { ActivatedRoute, Router } from '@angular/router'


@Component({
  selector: 'app-addcomp',
  templateUrl: './addcomp.component.html',
  styleUrls: ['./tables.component.scss']
})
export class AddcompComponent {
  data: any;
    sub: any;
    Items: any;

  constructor(private UserData: AllSearvicesService,private router: Router,private route : ActivatedRoute) {
    this.sub = this.route.params.subscribe(params => {
      this.Items = params;
      });
      console.log(this.Items);
  }
addCompany(FormData:any){
  console.log('FormData',FormData)
  if(this.Items.companyName){
    this.UserData.EditCompany(FormData,this.Items).subscribe((response:any)=>{
      console.log('Response Update Company ---->>>',response)
      if(response.message == 'Success'){
        this.router.navigate(['/companies']);
      }
    })
  }
  else{
    this.UserData.AddCompany(FormData).subscribe((response:any)=>{
      console.log('Response Add Company ---->>>',response)
      if(response.message == 'Success'){
        this.router.navigate(['/companies']);
      }
      // this.data=response
    })
  }
 
}
     ngOnInit() {
     
       
     }

    }
