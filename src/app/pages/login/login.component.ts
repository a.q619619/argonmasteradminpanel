import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AllSearvicesService } from 'src/app/all-searvices.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  constructor(private userData:AllSearvicesService,private router:Router) {}

  ngOnInit() {
  }
  ngOnDestroy() {
  }

  LoginUser(data:any) {
    console.log("data",data)
    this.userData.login(data).subscribe((response : any) => {
      console.log('Response Login User ---->>>',response)
      if(response.message === 'Success'){
        this.router.navigate(['/companies']);
      }

    })

    }

  }

