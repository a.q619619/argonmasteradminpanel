import { AddEmpolyeeComponent } from '../../pages/AddEmpolyee/addempolyee.component';
import { BillsComponent } from './../../pages/Bill-Histroy/bills.component';
import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { ServiceComponent } from '../../pages/Service/service.component'
import { AddServiceProviderComponent } from '../../pages/AddServiceProvider/addserviceprovider.component'
import { AddcompComponent } from '../../pages/AddCompany/addcomp.component'
import { SearchesComponent } from '../../pages/All-Searches/searches.component'
import { EmpolyeeComponent } from '../../pages/All-Empolyee/empolyee.component'
import { PasswordRequestComponent } from 'src/app/pages/Password_Request/passwordRequest.component';
import { SearchEmpolyeeComponent } from 'src/app/pages/SearchEmpolyee/searchEmpolyee.component';



export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'addcompany', component: AddcompComponent },
    { path: 'companies', component: TablesComponent },
    { path: 'addEmployee', component: AddEmpolyeeComponent },
    { path: 'serviceProvider', component: ServiceComponent },
    { path: 'passwordRequest', component: PasswordRequestComponent },
    { path: 'searchEmpolyee', component: SearchEmpolyeeComponent },
    { path: 'addserviceProvider', component: AddServiceProviderComponent },
    { path: 'billhistroy', component: BillsComponent },
    { path: 'searches', component: SearchesComponent },
    { path: 'empolyee/:id', component: EmpolyeeComponent }
];
