import { SearchEmpolyeeComponent } from './../../pages/SearchEmpolyee/searchEmpolyee.component';
import { AddEmpolyeeComponent } from '../../pages/AddEmpolyee/addempolyee.component';
import { AddServiceProviderComponent } from './../../pages/AddServiceProvider/addserviceprovider.component';
import { EmpolyeeComponent } from './../../pages/All-Empolyee/empolyee.component';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { AddcompComponent } from '../../pages/AddCompany/addcomp.component';
import { ServiceComponent } from '../../pages/Service/service.component';
import { SearchesComponent } from '../../pages/All-Searches/searches.component';
import { BillsComponent } from '../../pages/Bill-Histroy/bills.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TablesComponent,
    BillsComponent,
    ServiceComponent,
    SearchesComponent,
    IconsComponent,
    EmpolyeeComponent,
    AddEmpolyeeComponent,
    AddcompComponent,
    SearchEmpolyeeComponent,
    AddServiceProviderComponent,
    MapsComponent,

  ]
})

export class AdminLayoutModule {}
